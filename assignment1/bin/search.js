/**
 * Created by AbfalterJakob on 17.04.2017.
 */
const Searcher = require('../lib/Searcher');
const ScorerFactory = require('../lib/scoring/ScorerFactory');
const DAO = require('../lib/persistence/DAO');
const Config = require('../lib/util/config');
const QueryProcessor = require('../lib/io/QueryProcessor');
const BatchQueryProcessor = require('../lib/io/BatchQueryProcessor');
const readline = require('readline');
const logger = Config.logger;
const fs = require('fs');
const debug = require('../lib/util/debug');

let scorer = ScorerFactory.get(Config.function);
let qp = null;
if( Config.queryFile !== '' ) {
  logger.info("Got a Trec8 Topic file to read from");
  qp = new BatchQueryProcessor(Config.queryFile);
  start_search_process(qp, () => {});
}
else if( Config.query !== '' ) {
  logger.info("Got a query from command line argument");
  qp = new QueryProcessor(Config.query);
  start_search_process(qp, () => {});
}
else {
  logger.info("No query given by cmd arguments");
  const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
  });
  rl.question('Please provide a query... ', (answer) => {
    qp = new QueryProcessor(answer);
    start_search_process(qp, () => {
        process.exit(0);
    });
  });
}

function start_search_process(qp, done) {
    const searcher = new Searcher(qp, scorer);
    searcher.bootstrap((err, res) => {
        if (err) {
            logger.error("Failed to bootsrap searcher with error ", err);
        }
        else {
            logger.info('Starting a search');
            let start_time = debug.beginTimeMeasure();
            searcher.process_queries((err, results) => {
                if( err ) logger.error(`Error during searching`, err);
                if( results.length > 1 ) {
                    //console.log(BatchQueryProcessor.printableBatchQueryResult(results));
                    fs.open('batch_query_result_' + Config.function, 'w', 0o660, (err, fd) => {
                      for (let i = results.length; i > 0; i--) {
                        let index = results.length - i;
                        fs.writeSync(fd, results[index].toFormattedString(1000));
                      }
                      fs.closeSync(fd);
                    });
                }
                else {
                    console.log(results[0].toFormattedString());
                }
                logger.info(`Search took ${debug.endTimeMeaure(start_time)}`);
                done();
            });
        }
    });
}



