/**
 * Created by Jakob on 01.04.2017.
 */
const Indexer = require('../lib/Indexer');
const TREC8FileReader = require('../lib/io/TREC8FileReader');
const DAO = require('../lib/persistence/DAO');
const Config = require('../lib/util/config');
const logger = Config.logger;

logger.log('debug', "Root directory for files: " + Config.rootDir);

let indexer = new Indexer(
    new TREC8FileReader(Config.rootDir),
    Config.processing_sequence,
    new DAO()
);
indexer.build_index();
//indexer.persist_index();

