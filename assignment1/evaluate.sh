#!/bin/bash

if [ ! -f trec_eval/trec_eval ]
then
  cd trec_eval
  rm -rf trec_eval*
  wget http://trec.nist.gov/trec_eval/trec_eval_latest.tar.gz
  tar xvf trec_eval_latest.tar.gz
  cd trec_eval.9.0
  make
  mv trec_eval ..
  cd ../..
fi

rm evaluate/*_evaluation
for file in evaluate/*
do
  EVAL_FILENAME=${file}_evaluation
  trec_eval/trec_eval -q -m official -c trec_eval/qrels.trec8.adhoc.parts1-5 $file > $EVAL_FILENAME
done
