/**
 * Created by AbfalterJakob on 05.04.2017.
 */
const PostingList = require('../model/PostingList');
const sizeof = require('object-sizeof');
const Config = require('../util/Config');
const logger = Config.logger;
const fs = require('fs');
//const readline = require('readline');
const HashTable = require('hashtable');
const path = require('path');
const LineByLineReader = require('line-by-line');
const BtreeIndex = require('./BtreeIndex');
const async = require('async');

class DAO {

  constructor() {
      this.dictionary = new HashTable();
      this.index = new BtreeIndex();
      this.document_stats = new HashTable();
  }

  /**
   *
   * @param {function} cb
   */
  bootstrap(cb) {
    async.series(
        [
          (cb2) => {
            this.index.open(cb2)
          }
        ],
        (err, res) => {
          cb(err);
        }
    )
  }

  /**
   * Add statistics about a document into memory
   *
   * @param docid {string}
   * @param stats {{}}
   */
  add_document_stats(docid, stats) {
    this.document_stats.put(docid, stats);
  }

  /**
   * Adds a term to the dictionary
   *
   * @param term String
   * @param documentid String
   */
  add_term(term, documentid) {
    this.blocksize++;
    if (!this.dictionary.has(term)) {
      let postingList = new PostingList(term);
      postingList.add_documentid(documentid);
      this.dictionary.put(term, postingList);
    }
    else {
      this.dictionary.get(term).add_documentid(documentid);
    }
  }

  /**
   * @returns int size of dictionary in megabyte
   */
  get_list_size() {
    return this.dictionary.size();
  }

  store_posting_lists() {
    let files = fs.readdirSync(Config.blockdir);
    let fn = 1;
    for (let i = files.length - 1; i >= 0; i--) {
      let fni = parseInt(files[i]);
      if (!isNaN(fni) && fni >= fn)
        fn = fni + 1;
    }

    logger.log('info', "Persisting current in-memory index to disk " + fn);

    let fd = fs.openSync(Config.blockdir + '/' + fn, 'w', 0o660);

    let keys = this.dictionary.keys();
    keys.sort();

    for (let i = keys.length; i > 0; i--) {
      let index = keys.length - i;
      let key = keys[index];
      let value = this.dictionary.get(key);
      fs.appendFileSync(fd, value.marshall_docid_list());
    }

    this.dictionary.clear();
  }

  persist_index() {
    /*
     * obtain all block file names to read from
     */
    let block_files = fs.readdirSync(Config.blockdir);
    logger.info('block files', block_files);

    /*
     * initialize readline interfaces
     */
    let terms_read = 0;
    let line_readers = [];
    for (let i = block_files.length; i > 0; i--) {
      let index = block_files.length - i;
      let block_fn = block_files[index];
      let block_fp = path.normalize(path.join(Config.blockdir, block_fn));
      let lr = new LineByLineReader(block_fp);
      lr.on('error', err => logger.error(`error reading ${block_fn}: `, err));
      line_readers.push(lr);
    }

    let all_current_lines_read_cb = () => {
      let all_streams_ready = true;
      for (let i = line_readers.length - 1; i >= 0; i--) {
        if (!line_readers[i]._paused && !line_readers[i]._end || line_readers[i].dirty) {
          all_streams_ready = false;
          break;
        }
      }

      // determine whether any lines are left for processing
      let at_least_one_line_defined = false;
      for (let i = current_lines.length - 1; i >= 0; i--)
        if (current_lines[i] !== null) {
          at_least_one_line_defined = true;
          break;
        }

      if (all_streams_ready && at_least_one_line_defined) {
        // convert newly read lines into posting lists
        for (let i = readline_indices.length - 1; i >= 0; i--) {
          let index = readline_indices[i];
          if (current_lines[index] !== null)
            current_posting_lists[index] = PostingList.unmarshall_docid_list(current_lines[index]);
          else
            current_posting_lists[index] = null;
        }

        // map current terms to current posting list indices
        let current_terms = {};
        for (let i = current_posting_lists.length - 1; i >= 0; i--) {
          let cp = current_posting_lists[i];
          if (cp !== null) {
            let term = '__' + cp.term; // fix error in case of term is a reserved keyword
            if (current_terms[term])
              current_terms[term] = current_terms[term].concat([i]);
            else
              current_terms[term] = [i];
          }
        }

        // find "smallest" current term
        let smallest_current_term = null;
        for (let current_term in current_terms) {
          if (smallest_current_term === null || smallest_current_term !== null && current_term < smallest_current_term)
            smallest_current_term = current_term;
        }

        /*
         * the indices found denote the posting lists to be merged into the current
         * index and are subject to be replaced with a set of new posting lists
         */
        let current_pl_indices = current_terms[smallest_current_term];
        readline_indices = current_pl_indices;
        let merge_posting_lists = [];
        for (let i = current_pl_indices.length - 1; i >= 0; i--) {
          let index = current_pl_indices[i];
          merge_posting_lists.push(current_posting_lists[index]);
        }

        // perform merge_and_normalise of merge_posting_lists
        let merged_posting_list = PostingList.merge_and_normalise(merge_posting_lists);

        // write posting list back to index
        logger.debug(`Writing postings_list for term ${merged_posting_list.term} got df ${merged_posting_list.df}`);
        this.index.store_posting_list(
            merged_posting_list, terms_read,
            () => {
              logger.debug(`inserted pl "${merged_posting_list.term}" (df: ${merged_posting_list.df})`);
              terms_read++;
              if (terms_read % 1000 === 0) {
                logger.info(`Done merging ${terms_read} terms, current term ${merged_posting_list.term} (df: ${merged_posting_list.df})`);
              }

              /*
               * resume readline streams of blocks we processed the current line of and
               * read from these streams and iterate the current step
               */
              for (let i = readline_indices.length - 1; i >= 0; i--) {
                let index = readline_indices[i];
                /*
                 * necessary to mark resumed line readers as dirty - to fix the case of another
                 * line reader triggering processing via a delayed end event, interleaving with a
                 * delayed resume action -> on line/end the dirty mutex is cleared again
                 */
                line_readers[index].dirty = true;
                line_readers[index].resume();
              }
            }
        );
      }
    };

    /*
     * attach 'line' event handlers
     */
    // range from 0 to # of block_files - 1
    let readline_indices = [...new Array(block_files.length).keys()];
    let current_lines = [];
    let current_posting_lists = [];
    for (let i in line_readers) {
      line_readers[i].on('line', (line) => {
        line_readers[i].pause();
        line_readers[i].dirty = false;
        current_lines[i] = line;
        all_current_lines_read_cb();
      });
      line_readers[i].on('end', () => {
        line_readers[i].close();
        line_readers[i].dirty = false;
        current_lines[i] = null;
        all_current_lines_read_cb();
      });
    }
  }

  persist_doc_stats() {
    // Calculate collection specific stats
    const N = this.document_stats.size();
    let avglen = 0;
    let mavgtf = 0;
    const keys = this.document_stats.keys();
    for(let x = keys.length -1; x >= 0; x--) {
      let stat = this.document_stats.get(keys[x]);
      avglen += stat.len;
      mavgtf += stat.avgtf;
    }
    avglen = avglen / N;
    mavgtf = mavgtf / N;
    this.document_stats.put('collection', {
      N : N,
      avglen : avglen,
      mavgtf : mavgtf
    });

    // Write to json file
    fs.writeFileSync(Config.metadir + '/meta.json', JSON.stringify(this.document_stats.toJSON()));
    logger.info(`Wrote document stats to json, got N: ${N} avglen: ${avglen} mavgtf: ${mavgtf}`);

    // Clean up Hashtable
    this.document_stats.clear();
  }

  static read_doc_stats() {
    return JSON.parse(fs.readFileSync(Config.metadir + '/meta.json'));
  }


  retrieve_posting_list(term, cb) {
    this.index.get_posting_list(term, cb);
  }
}

module.exports = DAO;
