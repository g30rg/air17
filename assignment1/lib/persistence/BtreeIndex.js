const cadence = require('cadence');
const Strata = require('b-tree/strata');
const Config = require('../util/config');
const logger = Config.logger;
const PostingList = require('../model/PostingList');
const path = require('path');

let strata = undefined;
let strata_options = {
  directory: path.normalize(Config.datadir),
  leaf_size: 10000,
  branch_size: 1024
};

let create_strata = function (options) {
  if (strata === undefined) {
    strata = new Strata({
          directory: options.directory,
          leafSize: options.leaf_size,
          branchSize: options.branch_size,
          comparator: (term_a, term_b) => term_a < term_b ? -1 : term_a > term_b ? 1 : 0,
          extractor: (pl) => pl.term
        }
    );
  }
  return strata;
};

let create_btree = cadence(function (async, options) {
  let strata;
  async(
      () => {
        strata = create_strata(options);
        strata.create(async())
      },
      () => strata
  );
});

let open_btree = cadence(function (async, options) {
  let strata;
  async(
      () => {
        strata = create_strata(options);
        strata.open(async())
      },
      () => strata
  );
});

class BtreeIndex {
  open(cb) {
    let inner_cb = (error) => {
      if (error) {
        cb(error);
        return;
      }
      let optimize_predicate = (term_count) => term_count % 25000 === 0;
      this.insert = cadence((async, posting_list, term_count) => {
        async(
            () => {
              strata.mutator(posting_list.term, async());
            },
            (cursor) => {
              let success = false;
              if (cursor.index < 0) {
                cursor.insert(posting_list, posting_list.term, ~cursor.index);
                success = true;
              }
              else
                logger.warn(`failed to insert ${posting_list.term}, cursor index was ${cursor.index}, unlocking anyways...`);
              async(
                  () => {
                    cursor.unlock(async());
                  },
                  () => {
                    if (optimize_predicate(term_count))
                      strata.balance(async());
                  },
                  () => {
                    if (optimize_predicate(term_count))
                      strata.purge(0);
                    return success
                  }
              );
            }
        );
      });
      this.retrieve = cadence((async, term) => {
        async(
            () => {
              strata.iterator(term, async());
            },
            (cursor) => {
              let item = null;
              async(() => {
                item = cursor.page.items[cursor.offset];
                cursor.unlock(async());
                strata.purge(0);
              }, () => {
                return item === undefined ? null : item.record;
              });
            }
        );
      });
      cb();
    };

    open_btree(strata_options,
        (error) => {
          if (error) {
            logger.warn('failed to open index trying to create next ...', error);
            create_btree(strata_options,
                (error) => {
                  inner_cb(error)
                }
            );
          } else {
            inner_cb();
          }
        });
  }

  /**
   *
   * @param {PostingList} pl
   * @param term_count
   * @param {function} cb
   */
  store_posting_list(pl, term_count, cb) {
    let marshalled_pl = pl.marshall_posting_list();

    logger.debug(`inserting posting list with term ${pl.term}`);
    this.insert(marshalled_pl, term_count,
        (error, success) => {
          if (error) logger.error('caught error during insertion', error);
          if (success) logger.debug('successfully inserted');
          else logger.warn(`posting list already in index (term ${pl.term}`);
          cb(error);
        });
  }

  /**
   *
   * @param {string} term
   * @param {function} cb
   */
  get_posting_list(term, cb) {
    this.retrieve(term, (err, pl) => {
      if (err) logger.error('caught error retrieving', err);
      if (pl !== null) {
        pl = PostingList.unmarshall_posting_list(pl);
        logger.debug(`Successfully retrieved Postinglist for term "${term}"`);
      }
      else {
        logger.warn(`Failed to retrieve Postinglist for term "${term}"`);
      }
      cb(err, pl);
    });
  }
}

module.exports = BtreeIndex;
