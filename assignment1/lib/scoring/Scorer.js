const RankResult = require('../model/RankResult');
const ScoreResult = require('../model/ScoreResult');
const Config = require('../util/config');
const logger = Config.logger;

class Scorer {
    constructor() {
        this.setTag()
    }

    setTag() {
        this._tag = "generic_scorer";
    }

    get tag() {
        return this._tag;
    }

    score(postinglists, queryterms, searcher) {
        this.perform_static_caching(searcher);

        let scoreResults = [];
        for (let i = postinglists.length - 1; i >= 0; i--) {
            let pl = postinglists[i];
            if (pl !== null)
                for (let j = pl.postings.length - 1; j >= 0; j--) {
                    let posting = pl.postings[j];
                    let queryterm = queryterms[i];
                    let score = this.score_td(queryterm, posting, searcher, pl.df);
                    scoreResults.push(score);
                }
        }
        return Scorer.rank(scoreResults);
    }

    /**
     * Generate score for one particular term - document
     * (calculate weight of term, document combination)
     * @param term {QueryTerm}
     * @param posting {Posting}
     * @param searcher
     * @param df {number} document frequency of posting
     * @return {ScoreResult}
     */
    score_td(term, posting, searcher, df = 1) {
        return [];
    }

    /**
     *
     * @param scoreResults {[ScoreResult]}
     * @return {[RankResult]}
     */
    static rank(scoreResults) {
      let rankResults = [];

      // Sort by docid so that summing scoreresults works in N
      logger.info(`Scored ${scoreResults.length} document / term pairs`);
      scoreResults.sort(ScoreResult.comperator);

        /**
         * @type {RankResult}
         */
      let currentRankResult = null;
      let currentDocid = null;
      for(let i = scoreResults.length -1; i >= 0; i--) {
        const scoreResult = scoreResults[i];

        if( currentDocid === scoreResult.docid ) {
            // Same docid so increase score of current RankResult
            currentRankResult.increase_score(scoreResult.result);
        }
        else {
            // New document, push the current to resultlist and start new
            if( currentRankResult !== null )
                rankResults.push(currentRankResult);
            currentDocid = scoreResult.docid;
            currentRankResult = new RankResult(currentDocid, scoreResult.result);
        }
      }
      // Don't forget to add the last element when the loop finished
      if( currentRankResult !== null )
          rankResults.push(currentRankResult);

      // Sort by ScoreResult now
      rankResults.sort(RankResult.comperator);

      if( rankResults.length > 1000) {
          return rankResults.slice(0, 999);
      }
      else {
          return rankResults;
      }
    }

    /**
     *
     * @param {Searcher} searcher
     */
    perform_static_caching(searcher) { }
}

module.exports = Scorer;