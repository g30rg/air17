const Scorer = require('./Scorer');
const RankResult = require('../model/RankResult');
const Config = require('../util/Config');
const logger = Config.logger;

class BooleanAND extends Scorer {
  constructor() {
    super();
  }

  /**
   * @Override
   * @returns {*}
   */
  setTag() {
    this._tag = 'boolean_and';
  }

  score(postinglists, queryterms, searcher) {
    let scoreResults = [];

    // done - just return posting list as score result
    if (postinglists.length === 1) {
      let max = postinglists[0]._postings.length;
      for (let i = 0; i < max; i++)
        scoreResults.push(new RankResult(postinglists[0]._postings[i].posting_id, 0));
      return scoreResults;
    }

    let posting_indices = new Array(postinglists.length);
    posting_indices.fill(0);

    let get_posting = (pli) => {
      let pi = posting_indices[pli];
      return pli < postinglists.length && pi < postinglists[pli]._postings.length ? postinglists[pli]._postings[pi] : null;
    };

    let increment_posting_index = (pli_incr, pli_comp) => {
      let p_incr = get_posting(pli_incr);
      let p_comp = get_posting(pli_comp);

      // recursion termination criterion
      if (p_incr.posting_id >= p_comp.posting_id) return false;

      /*
       * recursion body
       */
      // do i have a skip?
      if (p_incr._skip_ptr !== -1) {
        if (p_incr._skip_did <= p_comp.posting_id) {
          posting_indices[pli_incr] = p_incr._skip_ptr;
          logger.debug('jump, jump! (erm i mean skip)');
        }
        else
          posting_indices[pli_incr]++;
      }
      else
        posting_indices[pli_incr]++;

      // recursion
      return true || increment_posting_index(pli_incr, pli_comp);
    };

    let run = true;
    while (run) {
      let max = postinglists.length - 1;
      let incremented = false;

      for (let i = 0; i < max; i++) {
        let successor = i + 1;
        /** @type {Posting} */
        let p_a = get_posting(i);
        /** @type {Posting} */
        let p_b = get_posting(successor);
        if (p_a !== null && p_b !== null) {
          if (p_a.posting_id < p_b.posting_id) {
            increment_posting_index(i, successor);
            incremented = true;
            break;
          } else if (p_a.posting_id > p_b.posting_id) {
            increment_posting_index(successor, i);
            incremented = true;
            break;
          }
        } else
          run = false;
      }

      /** @type {Posting} */
      let posting = get_posting(0);
      if (run && !incremented && posting !== null) {
        scoreResults.push(new RankResult(posting.posting_id, 0));
        posting_indices[0]++;
      }
    }

    return scoreResults;
  }
}

module.exports = BooleanAND;