/**
 * Created by AbfalterJakob on 17.04.2017.
 */
const Scorer = require('./Scorer');
const ScoreResult = require('../model/ScoreResult');

class TFIDF extends Scorer{

  /**
   * @Override
   */
  setTag() {
    return this._tag = "tfidf";
  }

  /**
   * @Override
   */
  score_td(term, posting, searcher, df) {
    const score = Math.log(1 + posting.tf) * Math.log(searcher.N / df);
    return new ScoreResult(term.token, score, posting.posting_id);
  }
}

module.exports = TFIDF;
