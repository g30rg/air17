/**
 * Created by AbfalterJakob on 21.04.2017.
 */
const Scorer = require('./Scorer');
const ScoreResult = require('../model/ScoreResult');

/** Fixed parameters for BM25Plus */
const k1 = 1.2;
const k3 = 100;

class BM25Plus extends Scorer {

    constructor() {
        super();
        this._coeff1 = undefined;
        this._cached2 = undefined;
        this._smoothed_number_of_docs = undefined;
    }

    /**
     *
     * @param {Searcher} searcher
     * @returns {*}
     */
    perform_static_caching(searcher) {
        super.perform_static_caching(searcher);

        // cache the first unchanged coefficient of equation (6) from the paper
        if (this._coeff1 === undefined)
            this._coeff1 = Math.pow(searcher.mavgtf, -2);

        // cache the second unchanged coefficient of equation (6) from the paper
        if (this._cached2 === undefined)
            this._cached2 = (1 - 1 / searcher.mavgtf) * searcher.N / searcher.avgL;

        if (this._smoothed_number_of_docs === undefined)
            this._smoothed_number_of_docs = searcher.N + 0.5;
    }

    /**
     * @Override
     */
    setTag(){
        this._tag = "bm25+";
    }

    /**
     * @Override
     */
    score_td(term, posting, searcher, df) {
        const B = this._coeff1 * searcher.get_avgTf(posting.posting_id) + this._cached2;

        const qtf_part = ((k3 + 1) * term.tf) / (k3 + term.tf);
        const docl_part = (k1 + 1) * posting.tf / (B + posting.tf);
        const bim_part = Math.log(this._smoothed_number_of_docs / (df + 0.5));

        const score = bim_part * qtf_part * docl_part;

        return new ScoreResult(term.token, score, posting.posting_id);
    }
}

module.exports = BM25Plus;
