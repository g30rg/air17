/**
 * Created by AbfalterJakob on 17.04.2017.
 */
const Scorer = require('./Scorer');
const ScoreResult = require('../model/ScoreResult');

/** Fixed params for BM25 */
const k1 = 1.2;
const b = 0.75;
const k3 = 100;

class BM25 extends Scorer {

    constructor() {
        super();
        this._one_minus_b = undefined;
        this._smoothed_number_of_docs = undefined;
    }

    /**
     * @Override
     */
    setTag(){
        this._tag = "bm25";
    }


    perform_static_caching(searcher) {
        super.perform_static_caching(searcher);

        if (this._one_minus_b === undefined)
            this._one_minus_b = (1 - b);

        if (this._smoothed_number_of_docs === undefined)
            this._smoothed_number_of_docs = searcher.N + 0.5;
    }

    /**
     * @Override
     */
    score_td(term, posting, searcher, df) {
        const K = k1 * (this._one_minus_b + b * (searcher.get_docL(posting.posting_id) / searcher.avgL));

        const qtf_part = ((k3 + 1) * term.tf) / (k3 + term.tf);
        const docl_part = (k1 + 1) * posting.tf / (K + posting.tf);
        const bim_part = Math.log(this._smoothed_number_of_docs / (df + 0.5));

        const score = bim_part * qtf_part * docl_part;

        return new ScoreResult(term.token, score, posting.posting_id);
    }
}

module.exports = BM25;
