const BM25 = require('./BM25');
const TFIDF = require('./TFIDF');
const BM25Plus = require('./BM25Plus');
const BooleanAND = require('./BooleanAND');

class ScorerFactory {
  /**
   }
   *
   * @param method
   * @returns {*}
   */
  static get(method) {
    switch (method.toUpperCase()) {
      case 'BM25':
        return new BM25();
        break;
      case 'TFIDF':
        return new TFIDF();
        break;
      case 'BM25+':
      case 'BM25PLUS':
        return new BM25Plus();
      case 'BOOLAND':
        return new BooleanAND();
      default:
        return null;
    }
  }
}

module.exports = ScorerFactory;
