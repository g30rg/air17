/**
 * Created by gursits on 01.04.2017.
 */

const fs = require('fs');
const path = require('path');
const recursive = require('recursive-readdir');
const htmlparser = require('htmlparser');
const Document = require('../model/Document');
const Config = require('../util/config');
const logger = Config.logger;

class TREC8FileReader {
  /**
   *
   * @param root_dir asdf
   */
  constructor(root_dir) {
    this.root_dir = root_dir;
  }

  /**
   *
   * @param cb
   */
  read_file_list(cb) {
    recursive(this.root_dir, (err, files) => {
      let found_files = files.length;
      logger.log('info', `Found ${found_files} files`);
      cb(files);
    });
  }

  parse_file(filepath) {
    let extract_text = (dom_node) => {
      let text = '';
      if (dom_node.type === 'tag' && dom_node.children)
        for (let i = dom_node.children.length - 1; i >= 0; i--)
          text = extract_text(dom_node.children[i]) + text;
      else if (dom_node.type === 'text')
        text = dom_node.raw + text;
      return text;
    };

    let content = fs.readFileSync(filepath);

    let handler = new htmlparser.DefaultHandler((err, dom) => {
      if (err) logger.log('error', err);
    });

    let parser = new htmlparser.Parser(handler);
    parser.parseComplete(content);

    let dom = handler.dom;
    let documents = [];
    for (let j = dom.length - 1; j >= 0; j--) {
      let doc = dom[j];
      if (doc.type === 'tag' && doc.name === 'DOC') {
        let docno = '';
        let meta = null;
        let text = '';
        for (let k = doc.children.length - 1; k >= 0; k--)
          if (doc.children[k].type === 'tag') {
            switch (doc.children[k].name) {
              case 'TEXT':
                text = extract_text(doc.children[k]);
                break;
              case 'DOCNO':
                docno = doc.children[k].children[0].raw;
                break;
              default:
                meta = doc.children[k];
                break;
            }
          }

        documents.push(new Document(docno, text, meta));
      }
    }
    return documents;
  }
}

module.exports = TREC8FileReader;
