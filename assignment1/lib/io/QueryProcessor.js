/**
 * Created by AbfalterJakob on 17.04.2017.
 */
const Query = require('../model/Query');

class QueryProcessor {
  /**
   *
   * @param query_term
   */
  constructor(query_term) {
    this._term = query_term;
  }

  /**
   * Return queries processed for the Topics document
   * @return {Query[]}
   */
  get_queries() {
    return [new Query(this._term)];
  }
}

module.exports = QueryProcessor;
