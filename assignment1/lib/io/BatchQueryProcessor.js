const Query = require('../model/Query');
const Trec8Query = require('../model/TREC8Query');
const fs = require('fs');
const r_topics = /<top>[\s\S]*?<\/top>/g;
const r_title = /<title>(.+)/i;
const r_desc = /<desc>.*\n([\S\s]*?)</i;
const r_narr = /<narr>.*\n([\S\s]*?)</i;
const r_num = /Number: (\d+)/i;

class BatchQueryProcessor {

    /**
     * @param results {RetrievalResult[]}
     */
  static printableBatchQueryResult(results) {
    let str = "";
    let max = results.length;
    for(let i = 0; i < max; i++) {
      let r = results[i];
      str += r.toFormattedString();
    }
    return str;
  }
  /**
   *
   * @param queries_file_path
   */
  constructor(queries_file_path) {
    let query_doc = fs.readFileSync(queries_file_path, 'utf-8');
    let topics = query_doc.match(r_topics);
    let topics_l = topics.length;
    this._queries = [];
    for( let i = 0; i < topics_l; i++ ) {
      let topic = topics[i];
      let title = topic.match(r_title)[1];
      let nr = topic.match(r_num)[1];
      let narr = topic.match(r_narr)[1];
      let desc = topic.match(r_desc)[1];
      this._queries.push(new Trec8Query(nr, title, desc, narr));
    }
  }

  /**
   *
   * @returns {Array|*}
   */
  get_queries() {
    return this._queries;
  }
}

module.exports = BatchQueryProcessor;