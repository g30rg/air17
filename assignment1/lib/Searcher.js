/**
 * Created by AbfalterJakob on 17.04.2017.
 */
const Config = require('../lib/util/config');
const DAO = require('./persistence/DAO');
const async = require('async');
const Scorer = require('./scoring/Scorer');
const RetrievalResult = require('./model/RetrievalResult');

const logger = Config.logger;

class Searcher {
  /**
   * @param queryProcessor {QueryProcessor|BatchQueryProcessor}
   * @param scorer {Scorer}
   */
  constructor(queryProcessor, scorer) {
    this.queryProcessor = queryProcessor;
    this.scorer = scorer;
    this.dao = new DAO();
    this._doc_stats = DAO.read_doc_stats();
    this._N = this._doc_stats.collection.N;
    this._avgL = this._doc_stats.collection.avglen;
    this._mavgtf = this._doc_stats.collection.mavgtf;
  }

  /**
   * Process search documents and load necessary modules
   */
  bootstrap(cb) {
    this.queries = this.queryProcessor.get_queries();
    async.series(
        [
          (cb2) => {
            this.dao.bootstrap(cb2);
          }
        ],
        (err, res) => {
          cb(err, res);
        }
    );
  }

  /**
   *
   * @param cb
   */
  process_queries(cb) {
    let qs = this.queries;
    let results = [];
    let query_tasks = [];
    for (let i = qs.length; i > 0; i--) {
      let index = qs.length - i;
      let q = qs[index];
      query_tasks.push(cb2 => {
        this.do_search(q, (err, result) => {
          results.push(new RetrievalResult(q.get_identifier()
              , result, 'grp7-' + this.scorer.tag));
          cb2(err);
        });
      });
    }
    async.series(query_tasks, (err) => {
      cb(err, results);
    });
  }

  /**
   *
   * @param query
   * @param cb
   */
  do_search(query, cb) {
    logger.info(`Starting search for "${query._query_text}"`);
    query.get_tokens((tokenlist) => {
      let postinglists = new Array(tokenlist.length);
      let tasks = [];
      logger.info("tokenlist", tokenlist);
      for (let i = tokenlist.length - 1; i >= 0; i--) {
        tasks.push((cb2) => {
          let queryTerm = tokenlist[i];
          this.dao.retrieve_posting_list(queryTerm.token, (err, result) => {
            if (err) {
              cb2(err);
            }
            else {
              cb2(null, {term: queryTerm, postinglist: result});
            }
          });
        });
      }
      async.parallel(tasks, (err, results) => {
            logger.info('Received postinglists');
            let queryterms = [];
            let postinglists = [];
            for (let i = 0; i < results.length; i++) {
              /** @type term {QueryTerm} */
              let term = results[i].term;
              /** @type pl {PostingList} */
              let pl = results[i].postinglist;
              queryterms.push(term);
              postinglists.push(pl);
              if (pl !== null)
                logger.debug(`term "${term.token}"`, pl.postings);
              else
                logger.debug(`term "${term.token}" -> no postings found`);
            }

            let rankResult = this.scorer.score(postinglists, queryterms, this);

            cb(null, rankResult);
          }
      )
    });
  }

  get N() {
    return this._N;
  }

  get avgL() {
    return this._avgL;
  }

  get mavgtf() {
      return this._mavgtf;
  }

    get_docL(posting_id) {
    return this._doc_stats[posting_id] ? this._doc_stats[posting_id].len : 0;
  }

  get_avgTf(posting_id) {
    return this._doc_stats[posting_id] ? this._doc_stats[posting_id].avgtf : 0;
  }
}

module.exports = Searcher;
