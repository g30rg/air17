/**
 * Created by AbfalterJakob on 19.04.2017.
 *
 * The Result of all ranked documents in regards to a query
 */

class RetrievalResult {

    /**
     * @param query {string} processed query
     * @param rankResults {[RankResult]} Results sorted by score
     * @param runName {string} name of this queryrun
     * @param topicid {number} if the query belongs to a specific topic
     */
    constructor(query, rankResults, runName, topicid = -1) {
        this._topicid = topicid;
        this._query = query;
        this._rankResults = rankResults;
        this._runName = runName;
    }

    toFormattedString(top_n_results = null) {
        let respStr = "";
        let resultsLength = this._rankResults.length;
        let query_identifier = this._topicid;
        if( query_identifier === -1 ) {
            query_identifier = this._query;
        }
        for(let i = 0; i < resultsLength && (top_n_results === null || top_n_results !== null && i < top_n_results); i++ ) {
            let r = this._rankResults[i];
            respStr += `${query_identifier} Q0 ${r.docid} ${r.score} ${i+1} ${this._runName}\r\n`;
        }
        return respStr;
    }

}

module.exports = RetrievalResult;
