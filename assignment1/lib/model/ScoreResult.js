/**
 * Created by AbfalterJakob on 19.04.2017.
 *
 * The result for one document in regards to one single queryterm
 */
class ScoreResult {

    constructor(term, result, docid) {
        this._term = term;
        this._result = result;
        this._docid = docid;
    }

    get term() {
        return this._term;
    }

    get result() {
        return this._result;
    }

    get docid() {
        return this._docid;
    }

    static get comperator() {
        /**
         * @param a {RankResult}
         * @param b {RankResult}
         */
        return function compare(a,b) {
            if( a.docid < b.docid ) return 1;
            if( a.docid > b.docid ) return -1;
            return 0;
        }
    }
}

module.exports = ScoreResult;
