/**
 * Created by AbfalterJakob on 17.04.2017.
 */
const Query = require('./Query');

class TREC8Query extends Query {
  constructor(num, title, desc, narr) {
    super(title);
    this._num = num;
    this._title = title;
    this._desc = desc;
    this._narr = narr;
  }

  get_identifier() {
    return this._num;
  }

  get num() {
    return this._num;
  }

  get title() {
    return this._title;
  }

  get desc() {
    return this._desc;
  }

  get narr() {
    return this._narr;
  }
}

module.exports = TREC8Query;