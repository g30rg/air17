/**
 * Created by AbfalterJakob on 05.04.2017.
 */
const Posting = require('./Posting');

class PostingList {
  /**
   * @param term string
   */
  constructor(term) {
    this.term = term;
    this._documentids = [];
    this._postings = [];
  }

  /**
   * add a document to the posting list
   *
   * @param documentid string
   */
  add_documentid(documentid) {
    //this.documentids.insertOne(docid);
    this._documentids.push(documentid);
  }

  set documentids(documentids) {
    this._documentids = documentids;
  }

  get documentids() {
    this._documentids.sort();
    return this._documentids;
  }

  /**
   *
   * @param {Posting} posting
   */
  add_posting(posting) {
    this._postings.push(posting);
  }

  get postings() {
    return this._postings;
  }

  get df() {
    return this._postings.length;
  }

  marshall_docid_list() {
    return this.term + ' ' + this.documentids.join(',') + '\n';
  }

  static unmarshall_docid_list(string) {
    let [key, val] = string.trim().split(' ');
    let ps = new PostingList(key);
    ps.documentids = val.split(',');
    return ps;
  }

  marshall_posting_list() {
    let o = {term: this.term, postings: []};
    for (let i = this._postings.length; i > 0; i--) {
      let index = this._postings.length - i;
      o.postings.push(this._postings[index].marshall());
    }
    return o;
  }

  static unmarshall_posting_list(o) {
    let pl = new PostingList(o.term);
    for (let i = o.postings.length; i > 0; i--) {
      let index = o.postings.length - i;
      pl.add_posting(Posting.unmarshall(o.postings[index]));
    }
    return pl;
  }

  /**
   * assume document ids stored in posting lists passed in as argument
   * are sorted
   * @param {PostingList[]} posting_lists
   * @returns {null}
   */
  static merge_and_normalise(posting_lists) {
    if (posting_lists.length === 0) return null;

    // determine term frequency
    let posting_ptrs = [];
    for (let i = posting_lists.length - 1; i >= 0; i--)
      posting_ptrs[i] = 0;

    let find_smallest_doc_id = function (curr_smallest_term = null) {
      let smallest_term = null;
      for (let i = posting_ptrs.length - 1; i >= 0; i--) {
        let pptr = posting_ptrs[i];
        let doc_ids = posting_lists[i]._documentids;
        if (pptr < doc_ids.length) {
          let doc_id = doc_ids[pptr];
          if (curr_smallest_term !== null && doc_id === curr_smallest_term)
            return curr_smallest_term;
          if (smallest_term === null || smallest_term !== null && doc_id < smallest_term)
            smallest_term = doc_id;
        }
      }
      return smallest_term;
    };

    // normalize postinglist (sort and remove doubles)
    let merged_ps = new PostingList(posting_lists[0].term);

    let run = true;
    let smallest_docid = find_smallest_doc_id();
    let posting = new Posting(smallest_docid);
    while (run) {
      // run through all current ptrs
      run = false;
      for (let i = posting_ptrs.length - 1; i >= 0; i--) {
        let pptr = posting_ptrs[i];
        let doc_ids = posting_lists[i]._documentids;
        // increment for every ptr equal to min
        if (doc_ids.length > pptr) {
          run = true;
          if (smallest_docid === doc_ids[pptr]) {
            posting.increment_tf();
            posting_ptrs[i]++;
          }
        }
      }

      /*
       * cycle current posting to a new one if a new smallest term
       * was found
       */
      let new_smallest_docid = find_smallest_doc_id(smallest_docid);
      if (new_smallest_docid !== smallest_docid) {
        merged_ps.add_posting(posting);
        smallest_docid = new_smallest_docid;
        posting = new Posting(smallest_docid);
      }
    }

    // Set Skip Pointers
    let skip_size = parseInt(Math.sqrt(merged_ps._postings.length));
    for(let i = skip_size; i < merged_ps._postings.length; i+=skip_size) {
      let skip_index = i;
      let skip_value = merged_ps._postings[i]._posting_id;
      let pointer = i - skip_size;
      merged_ps._postings[pointer].skip = {
        ptr : skip_index,
        did : skip_value
      }
    }

    return merged_ps;
  }
}

module.exports = PostingList;
