const QueryTerm = require('./QueryTerm');
const Config = require('../util/config');
const logger = Config.logger;

class Query {
  constructor(term) {
    this._query_text = term;
    this._processingSequence = Config.processing_sequence;
  }

  get_identifier() {
    return this._query_text;
  }

  get query_text() {
    return this._query_text;
  }

  get_tokens(cb) {
    this._processingSequence.execute(this.query_text, (err, tokens) => {
      if(err) {
        logger.error("Failed getting tokens out of query text", err);
      }
      else {
        /**
         * Transform into valid queryterms
         * @type {[QueryTerm]}
         */
        let querytokens = [];
        for(let j = tokens.length -1; j >= 0; j--) {
            let found = false;
            let token = tokens[j].src;
            logger.debug(`Query token: ${token}`);
            for(let x = querytokens.length -1; x >= 0; x--) {
                if( querytokens[x].token === token ) {
                    querytokens[x].inc_tf();
                    found = true;
                    break;
                }
            }
            if( !found ) {
                querytokens.push(new QueryTerm(token));
            }
        }

        cb(querytokens);
      }
    });
  }
}

module.exports = Query;