/**
 * Created by Jakob on 01.04.2017.
 */
const count = require('word-count');

class Document {
  get meta() {
    return this._meta;
  }

  get text() {
    return this._text;
  }

  get id() {
    return this._id;
  }

  /**
   *
   * @param id
   * @param text
   * @param meta
   * @constructor
   */
  constructor(id, text, meta) {
    this._id = id || null;
    this._text = text || "";
    this._meta = meta || {};
    if (this._id !== null && this._id.trim) this._id = this._id.trim();
    this._text = this._text.trim();
    this._meta.len = count(this._text);
  }
}

module.exports = Document;

