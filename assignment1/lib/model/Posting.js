class Posting {
  constructor(posting_id) {
    this._posting_id = posting_id;
    this._tf = 0;
    this._skip_ptr = -1;
    this._skip_posting = undefined;
  }

  increment_tf() {
    this._tf++;
  }

  set skip(skip) {
    this._skip_ptr = skip.ptr;
    this._skip_did = skip.did;
  }

  get skip() {
    return {ptr: this._skip_ptr, did: this._skip_did};
  }

  get has_skip() {
    return this._skip_ptr !== -1;
  }


  get posting_id() {
      return this._posting_id;
  }

  get tf() {
      return this._tf;
  }

  marshall() {
    let data = {
      did: this._posting_id,
      tf: this._tf
    };
    if (this.has_skip) {
      data.skip = {ptr: this._skip_ptr, did: this._skip_did};
    }
    return data;
  }

  static unmarshall(o) {
    if (!o.did) return null;
    let posting = new Posting(o.did);
    posting._tf = o.tf;
    if (o.skip)
      posting.skip = {ptr: o.skip.ptr, did: o.skip.did};
    return posting;
  }
}

module.exports = Posting;
