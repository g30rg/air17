/**
 * Created by AbfalterJakob on 19.04.2017.
 */
class QueryTerm {
    constructor(token) {
        this._token = token;
        this._tf = 1;
    }

    inc_tf() {
        this._tf++;
    }

    get token() {
        return this._token;
    }

    get tf() {
        return this._tf;
    }
}

module.exports = QueryTerm;
