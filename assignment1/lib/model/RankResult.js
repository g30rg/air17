/**
 * Created by AbfalterJakob on 19.04.2017.
 *
 * The result of one document in regards to a complete query
 */

class RankResult {

    constructor(docid, score) {
        this._docid = docid;
        this._score = score;
    }

    increase_score(value) {
        this._score += value;
    }

    get docid() {
        return this._docid;
    }

    get score() {
        return this._score;
    }

    static get comperator() {
        /**
         * @param a {RankResult}
         * @param b {RankResult}
         */
        return function compare(a,b) {
            if( a.score < b.score ) return 1;
            if( a.score > b.score ) return -1;
            return 0;
        }
    }
}

module.exports = RankResult;