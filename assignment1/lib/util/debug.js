module.exports = {
  beginTimeMeasure: () => process.hrtime(),
  endTimeMeaure: (start) => {
    let elapsed = process.hrtime(start);
    return elapsed + " s";
  }
};