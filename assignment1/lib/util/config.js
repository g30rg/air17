const commandLineArgs = require('command-line-args');
const winston = require('winston');
const PreprocessingSequence = require('./../preprocessing/PreprocessingSequence');
const EntQuoteDecoder = require('./../preprocessing/EntQuoteDecoder');
const Tokenizer = require('./../preprocessing/Tokenizer');
const CaseFolder = require('./../preprocessing/CaseFolder');
const SpecialChars = require('./../preprocessing/SpecialChars');
const StopWordFilter = require('./../preprocessing/StopWordFilter');
const Stemmer = require('./../preprocessing/Stemmer');
const Lemmatizer = require('./../preprocessing/Lemmatizer');

const options = commandLineArgs([
  {name: 'src', alias: 's', type: String, defaultValue: '../resources/Adhoc/'},
  {name: 'casefold', alias: 'c', type: Boolean, defaultValue: false},
  {name: 'stopword', alias: 'w', type: Boolean, defaultValue: false},
  {name: 'prestemmer', alias: 'p', type: Boolean, defaultValue: false},
  {name: 'stemmer', alias: 't', type: Boolean, defaultValue: false},
  {name: 'lemmatizer', alias: 'l', type: Boolean, defaultValue: false},
  {name: 'defaultprocessing', alias: 'd', type: Boolean, defaultValue: false},
  {name: 'verbosity', alias: 'v', type: String, defaultValue: 'info'},
  {name: 'blockdir', type: String, defaultValue: 'blocks'},
  {name: 'datadir', type: String, defaultValue: 'data'},
  {name: 'metadir', type: String, defaultValue: 'meta'},
  {name: 'function', alias: 'f', type: String, defaultValue: 'bm25'},
  {name: 'queryfile', type: String, defaultValue: ''},
  {name: 'query', alias: 'q', type: String, defaultValue: ''}
]);

winston.info("Reading arguments...", options);

let rootDir = options.src;
let doCaseFold = options.casefold;
let doStopWord = options.stopword;
let doPreStemming = options.prestemmer;
let doStemming = options.stemmer;
let doLemmetizing = options.lemmatizer;
if (options.defaultprocessing) {
  doCaseFold = true;
  doStopWord = true;
  doPreStemming = true;
  doStemming = true;
}

let scoringFunction = options.function;
let queryFile = options.queryfile;
let query = options.query;

let verbosity = options.verbosity;
if (Object.keys(winston.config.cli.levels).indexOf(verbosity) === -1)
  verbosity = 'info';
const logger = winston;
logger.level = verbosity;
logger.info(`Read options:
  casefolding: ${doCaseFold},
  stopword: ${doStopWord},
  prestemmer: ${doPreStemming},
  stemmer: ${doStemming},
  lemmer: ${doLemmetizing},
  default processing: ${options.defaultprocessing},
  verbosity: ${verbosity},
  function: ${scoringFunction},
  queryFile: ${queryFile},
  query: ${query}`);

let blockdir = options.blockdir;
let datadir = options.datadir;
let metadir = options.metadir;

class Config {
  static get logger() {
    return logger;
  }

  static get rootDir() {
    return rootDir;
  }

  static get doLemmetizing() {
    return doLemmetizing;
  }

  static get doCaseFold() {
    return doCaseFold;
  }

  static get doStopWord() {
    return doStopWord;
  }

  static get doStemming() {
    return doStemming;
  }

  static get doPreStemming() {
    return doPreStemming;
  }

  static get blockdir() {
    return blockdir;
  }

  static get metadir() {
    return metadir
  }

  static get datadir() {
    return datadir;
  }

  static get queryFile() {
    return queryFile;
  }

  static get query() {
    return query;
  }

  static get function () {
    return scoringFunction;
  }

  static get processing_sequence() {
    let ps = new PreprocessingSequence();
    ps.add_preprocessor(EntQuoteDecoder.get_action());
    ps.add_preprocessor(Tokenizer.get_action());

    if (Config.doCaseFold)
      ps.add_preprocessor(CaseFolder.get_action());
    if (Config.doStopWord)
      ps.add_preprocessor(StopWordFilter.get_action());
    if (Config.doPreStemming)
      ps.add_preprocessor(SpecialChars.get_action());
    if (Config.doStemming)
      ps.add_preprocessor(Stemmer.get_action());
    if (Config.doLemmetizing)
      ps.add_preprocessor(Lemmatizer.get_action());

    return ps;
  }
}

module.exports = Config;
