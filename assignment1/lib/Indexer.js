/**
 * Created by Jakob on 01.04.2017.
 */

const Config = require('./util/Config');
const logger = Config.logger;
const async = require('async');
const debug = require('./util/debug');

const MAX_BLOCK_SIZE = 100000;

class Indexer {
  /**
   *
   * @param file_reader currently only TREC8FileReader, potentially interface this
   * @param preprocessing_sequence instance of class {@link PreprocessingSequence}
   * @param dao instance of class {@link DAO}
   */
  constructor(file_reader, preprocessing_sequence, dao) {
    this.file_reader = file_reader;
    this._preprocessing_sequence = preprocessing_sequence;
    this.dao = dao;
  }

  /**
   *
   * @param document
   * @param cb
   */
  indexDocument(document, cb) {
    this._preprocessing_sequence.execute(
        document.text,
        (err, terms) => {
          if (err) {
            logger.log('error', 'failed to process document', document, 'with error', err);
          }
          else {
            logger.log('debug', `document ${document.id} ready for indexing, acquired ${terms.length} terms`);
            let doclen = document.meta.len;
            terms.sort((a,b) => a.src < b.src ? -1 : a.src === b.src ? 0 : 1);

            let avgtf = 0;
            let current_term = terms.length === 0 ? '' : terms[0].src;
            let max = terms.length;
            let tf = 1;
            let distinct_term_count = 1;

            for (let i = 1; i < max; i++) {
                let t = terms[i];
                if( t.src !== current_term ) {
                    current_term = t.src;
                    avgtf += tf;
                    tf = 1;
                    distinct_term_count++;
                }
                else {
                    tf++;
                }
                this.dao.add_term(t.src, document.id);
            }
            if( terms.length > 0 )
                avgtf += tf;
            if( distinct_term_count > 0 )
                avgtf = avgtf / distinct_term_count;
            
            this.dao.add_document_stats(document.id, {
              len : doclen,
              avgtf : avgtf
            });

            if (this.dao.get_list_size() > MAX_BLOCK_SIZE) {
              this.dao.store_posting_lists();
            }
            else {
              logger.log('debug', `Current in memory index size ${this.dao.get_list_size()}`)
            }
            logger.log('debug', "Finished indexing a document");
            cb();
          }
        }
    )
  }

  bootstrap(cb) {
    async.series(
        [
          (cb) => {
            this.dao.bootstrap(cb);
          }
        ],
        (err, res) => {
          cb(err, res)
        }
    );
  }

  build_index() {
    let initial_start = debug.beginTimeMeasure();
    let tmp_start = debug.beginTimeMeasure();

    this.bootstrap((err, res) => {
      if (err) {
        logger.error('failed to start building the index', err);
        return;
      }

      /**
       * request a file list of all files known to the file reader
       */
      this.file_reader.read_file_list((file_list) => {
        /**
         * define a list of async file processing tasks
         * @type {Array}
         */
        let file_tasks = [];

        for (let i = file_list.length - 1; i >= 0; i--) {
          let filepath = file_list[i];
          /**
           * create the corresponding task per file
           */
          file_tasks.push((cb) => {
            let doc_tasks = [];
            let documents = this.file_reader.parse_file(filepath);

            /**
             * for every document define the indexing task
             * executed in sequence whose end concludes the
             * file task defining defined by its document tasks
             */
            for (let j = documents.length - 1; j >= 0; j--) {
              doc_tasks.push((cb2) => {
                this.indexDocument(documents[j], cb2);
              });
            }

            /**
             * execute the doc tasks series and trigger cb
             * -> kicking off the next file task in the outer sequence
             */
            async.series(doc_tasks, (err, res) => {
              let consumed = debug.endTimeMeaure(tmp_start);
              let total_consumed = debug.endTimeMeaure(initial_start);
              tmp_start = debug.beginTimeMeasure();
              logger.info(`[${file_list.length - i}/${file_list.length} processed ${doc_tasks.length} documents from file ${filepath} (took: ${consumed}, total: ${total_consumed})`);
              cb();
            });
          });
        }

        /**
         * kick off processing all files
         * when done processing all files perform one final storage operation and start writing the optimized index
         * (step from blocks to b*-tree)
         */
        async.series(file_tasks, (err, res) => {
          this.dao.store_posting_lists();
          this.persist_index();
          this.dao.persist_doc_stats();
        });
      });
    });
  }

  /**
   * TODO remove me
   */
  persist_index() {
    this.bootstrap((err, res) => {
      this.dao.persist_index()
    });
  }
}

module.exports = Indexer;