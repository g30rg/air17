/**
 * Created by gursits on 01.04.2017.
 */

const tokenizer2 = require('tokenizer2');
const Readable = require('stream').Readable;

class Tokenizer {
  /**
   *
   * @returns {function(*, *)}
   */
  static get_action() {
    return function (text, callback) {
      const Config = require('../util/Config');
      Config.logger.log('debug', 'start tokenization');
      let tokens = [];
      let token_stream = tokenizer2();
      token_stream.addRule(/^[\s]+$/, 'whitespace');
      token_stream.addRule(/^[^\s]+$/, 'term');
      token_stream.on('data', token => {
        if (token.type === 'term')
          tokens.push(token);
      });
      token_stream.on('end', () => {
        Config.logger.log('debug', 'done tokenization');
        callback(null, tokens);
      });

      /*
       * stream needed for performance of tokenisation
       * null needed to terminate the stream and kick off 'end' events
       * => implies create a new readable stream per tokenisation
       */
      let stream = new Readable();
      stream._read = () => {
      };
      stream.pipe(token_stream);
      stream.push(text);
      stream.push(null);
    };
  }
}

module.exports = Tokenizer;
