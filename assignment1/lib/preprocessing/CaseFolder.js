/**
 * Created by Jakob on 02.04.2017.
 */
const async = require('async');

class CaseFolder {
  /**
   *
   * @returns {function(*, *)}
   */
  static get_action() {
    /**
     * @param tokens []
     * @param callback function (*, *)
     */
    return function (tokens, callback) {
      const Config = require('../util/Config');
      Config.logger.log('debug', "Case folding tokens");
      for (let i = tokens.length - 1; i >= 0; i--) {
        tokens[i].src = tokens[i].src.toLowerCase();
      }
      async.setImmediate(() => {
        callback(null, tokens);
      });
    }
  }
}

module.exports = CaseFolder;