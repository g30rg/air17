/**
 * Created by Jakob on 08.04.2017.
 */
const async = require('async');
const Lemmer = require('lemmer');

class Lemmatizer {

  /**
   * @return {Function}
   */
  static get_action() {
    return function (tokens, callback) {
      const Config = require('../util/Config');
      Config.logger.log('debug', 'Lemmetizing tokens');
      let todo = tokens.length;
      for (let j = todo - 1; j >= 0; j--) {
        let t = tokens[j].src;
        Lemmer.lemmatize(t, (err, word) => {
          tokens[j].src = word;
          todo--;
          if (todo <= 0) {
            async.setImmediate(() => {
              callback(null, tokens);
            });
          }
        });
      }
    }
  }
}

module.exports = Lemmatizer;