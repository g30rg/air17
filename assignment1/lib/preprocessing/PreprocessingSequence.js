/**
 * Created by gursits on 01.04.2017.
 */

const async = require('async');

class PreprocessingSequence {
  /**
   *
   */
  constructor() {
    this._preprocessors = [];
  }

  /**
   *
   * @param preprocessor
   */
  add_preprocessor(preprocessor) {
    this._preprocessors.push(preprocessor);
  }

  /**
   *
   * @param text
   * @param cb
   * @returns {*}
   */
  execute(text, cb) {
    async.waterfall(
        [(callback) => {
          async.setImmediate(() => {
            callback(null, text)
          });
        }].concat(this._preprocessors),
        (err, result) => {
          cb(err, result)
        });
  }
}

module.exports = PreprocessingSequence;
