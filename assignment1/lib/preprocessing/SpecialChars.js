/**
 * Created by Jakob on 16.04.2017.
 */
const async = require('async');
const regex = /[^a-zA-Z0-9\-%.+]+/g;

class SpecialChars {

    static get_action() {
       return function (tokens, callback) {
           const Config = require('../util/Config');
           Config.logger.log('debug', "Handling special characters");
           let preStemmedTokens = [];
           for(let i = tokens.length -1; i >= 0; i--) {
               let t = tokens[i];
               let trimmed = t.src.replace(regex, '');
               if( trimmed !== '' ) {
                   Config.logger.debug(`Prestemmed token ${t.src} to ${trimmed[0]}`);
                   t.src = trimmed;
                   preStemmedTokens.push(t);
               }
               else {
                   Config.logger.debug(`Removing token ${t.src} during prestemming`);
               }
           }
           async.setImmediate(() => {
               callback(null, preStemmedTokens);
           });
       }
    }
}

module.exports = SpecialChars;