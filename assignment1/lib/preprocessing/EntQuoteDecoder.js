/**
 * Created by gursits on 01.04.2017.
 */

const entities = require('entities');
const async = require('async');

class EntQuoteDecoder {
  /**
   *
   * @returns {function(*=, *)}
   */
  static get_action() {
    return function (text, callback) {
      const Config = require('../util/Config');
      Config.logger.log('debug', 'decoding quotes');
      text = entities.decodeHTML(text);
      async.setImmediate(() => {
        callback(null, text);
      });
    };
  }
}

module.exports = EntQuoteDecoder;
