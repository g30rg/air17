/**
 * Created by AbfalterJakob on 03.04.2017.
 */
const async = require('async');
const pstemmer = require('porter-stemmer').stemmer;

class Stemmer {

  /**
   *
   * @returns {Function}
   */
  static get_action() {
    return function (tokens, callback) {
      const Config = require('../util/Config');
      Config.logger.log('debug', "Stemming tokens");
      for (let i = tokens.length - 1; i >= 0; i--) {
        tokens[i].src = pstemmer(tokens[i].src);
      }
      async.setImmediate(() => {
        callback(null, tokens);
      })
    }
  }
}

module.exports = Stemmer;