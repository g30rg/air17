/**
 * Created by Jakob on 02.04.2017.
 */
const async = require('async');
const stopwords = require('stopwords').english;

class StopWordFilter {
  /**
   *
   * @returns {function(*, *)}
   */
  static get_action() {
    /**
     * @param tokens []
     * @param callback function(*, *)
     */
    return function (tokens, callback) {

      const Config = require('../util/Config');
      Config.logger.log('debug', "Removing stop words");
      let tokensWithoutStopWords = [];
      for (let i = tokens.length - 1; i >= 0; i--) {
        let t = tokens[i];

        // Add only tokens which are not present in stopwords list
        if (stopwords.indexOf(t.src) === -1) {
          tokensWithoutStopWords.push(t);
        }
      }
      Config.logger.log('debug', "Shrinked size of tokens from " + tokens.length + " to " + tokensWithoutStopWords.length);
      async.setImmediate(() => {
        callback(null, tokensWithoutStopWords);
      });
    }
  }
}

module.exports = StopWordFilter;

