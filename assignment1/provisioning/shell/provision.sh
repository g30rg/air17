#!/bin/bash

# Install utils
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install vim
sudo apt-get -y install unzip
sudo apt-get -y install tmux
sudo apt-get -y install dos2unix
sudo apt-get -y install htop
find /vagrant -path /vagrant/node_modules -prune -o -type f -name '*.sh' -exec dos2unix '{}' +

if hash node 2>/dev/null; then
  echo "nodejs already installed, nothing to do..."
else
  sudo apt-get -y update
  sudo apt-get -y upgrade
  curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
  sudo apt-get -y install npm
  sudo apt-get -y install nodejs
  sudo ln -s /usr/bin/nodejs /usr/bin/node
  sudo npm install -y pm2 -g
  sudo npm install -y mocha -g
fi

if hash g++ 2>/dev/null; then
  echo "g++ already installed, nothing to do..."
else
  sudo apt-get -y update
  sudo apt-get -y upgrade
  sudo apt-get -y install g++
fi